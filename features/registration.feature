Feature: Registration
As an admin I'd like to validate passwords and emails and ensure they are human by means of a captcha

  Scenario: I try to register with a compliant password
    When I try to register with password "Passw0rd!"
    Then I should be notified that the registration was succesful

  Scenario: I try to register with too short a password
    When I try to register with password "Ab34567"
    Then I should be notified that the password is too short

  Scenario: I try to register with a password missing uppercase letters
    When I try to register with password "a2345678"
    Then I should be notified that the password must contain at least one uppercase letter

  Scenario: I try to register with a password missing lowercase letters
    When I try to register with password "A2345678"
    Then I should be notified that the password must contain at least one lowercase letter

  Scenario: I try to register with a password missing numbers
    When I try to register with password "Abcdefgh"
    Then I should be notified that the password must contain at least one number

  Scenario: I try to register with an existing email address
    Given There is a registered user with email "ejemplo2@test.com"
    When I try to register with the email "ejemplo2@test.com"
    Then I should be notified that the email is already registered and that the registration failed

  Scenario: user registration with captcha failed
    When I fail to complete the captcha
    Then the registration fails

  Scenario: user registration with captcha success
    When I complete the captcha
    Then the registration succeeds
