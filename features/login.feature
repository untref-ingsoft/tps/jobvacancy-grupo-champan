Feature: Login features
As a user, my account is blocked after 3 consecutive failed attempts

  Scenario: I am blocked after 3 consecutive failed log in attempts
    Given I'm registered with email "pepito@validemail.com" and my password is "Passw0rd!1"
    When I log in with the email "pepito@validemail.com" and password "N0tmyp4ss" 3 times
    Then I should be notified that my account has been blocked for 24 hours
    And I shouldn't be able to login

  Scenario: I am not blocked after 2 failed login attempts
    Given I'm registered with email "pepito@validemail.com" and my password is "Passw0rd!1"
    When I log in with the email "pepito@validemail.com" and password "N0tmyp4ss" 2 times
    Then I login with email "pepito@validemail.com" and password "Passw0rd!1" succesfuly


  Scenario: A succesful login resets the counter
    Given I'm registered with email "pepito@validemail.com" and my password is "Passw0rd!1"
    And I log in with the email "pepito@validemail.com" and password "N0tmyp4ss" 2 times
    And I login with email "pepito@validemail.com" and password "Passw0rd!1"
    And I log out
    When I log in with the email "pepito@validemail.com" and password "N0tmyp4ss" 2 times
    Then I login with email "pepito@validemail.com" and password "Passw0rd!1" succesfuly

  Scenario: Loging after 24 hours with locked account
    Given I'm registered with email "pepito@validemail.com" and my password is "Passw0rd!1"
    And My account was blocked more than 24 hours ago
    When I log in with the email "pepito@validemail.com" and password "Passw0rd!1"
    Then I login succesfuly
