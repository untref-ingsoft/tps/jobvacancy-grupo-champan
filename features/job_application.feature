Feature: Job Application
  In order to get a job
  As a candidate
  I want to apply to an offer

  Background:
    Given only a "Web Programmer" offer exists in the offers list

  Scenario: Apply to job offer and provide a link to my CV
    Given I am logged in
    When I access the offers list page
    Then I provide "https://www.linkedin.com/in/juan-carlos-cp/" as a link to my CV

  Scenario: Apply to job offer while logged in
    Given I am logged in
    And I access the offers list page
    When I apply to the "Web programmer" offer
    And I fill the required information and apply
    Then I should receive a mail with offerer info

  Scenario: Apply to job offer while not logged in
    Given I am not logged in
    And I access the offers list page
    When I apply to the "Web programmer" offer
    Then I should be redirected to the login page
