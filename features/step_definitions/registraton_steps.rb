When('I try to register with password {string}') do |password|
  visit '/register'
  fill_in('user[name]', with: 'Elon Musk')
  fill_in('user[email]', with: 'elon@musk.com')
  fill_in('user[password]', with: password)
  fill_in('user[password_confirmation]', with: password)
  click_button('Create')
end

Then('I should be notified that the registration was succesful') do
  page.should have_content('User created')
end

Then('I should be notified that the password is too short') do
  page.should have_content('The password must be at least 8 characters long')
end

Then('I should be notified that the password must contain at least one uppercase letter') do
  page.should have_content('Please use at least 1 uppercase letter')
end

Then('I should be notified that the password must contain at least one lowercase letter') do
  page.should have_content('Please use at least 1 lowercase letter')
end

Then('I should be notified that the password must contain at least one number') do
  page.should have_content('Please use at least 1 number')
end

Given('There is a registered user with email {string}') do |email|
  visit '/register'
  fill_in('user[name]', with: 'Elon Musk')
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: 'Passw0rd!')
  fill_in('user[password_confirmation]', with: 'Passw0rd!')
  click_button('Create')
end

When('I try to register with the email {string}') do |email|
  visit '/register'
  fill_in('user[name]', with: 'Joseph Mercie')
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: 'Passw0rd!1')
  fill_in('user[password_confirmation]', with: 'Passw0rd!1')
  click_button('Create')
end

Then('I should be notified that the email is already registered and that the registration failed') do
  page.should have_content('Registration failed because the email is already registered')
end

When('I fail to complete the captcha') do
  Recaptcha.configuration.skip_verify_env.delete('test')
  visit '/register'
  fill_in('user[name]', with: 'Joseph Mercie')
  fill_in('user[email]', with: 'joseph@mercie.com')
  fill_in('user[password]', with: 'Passw0rd!1')
  fill_in('user[password_confirmation]', with: 'Passw0rd!1')
end

Then('the registration fails') do
  click_button('Create')
  page.should have_content('reCAPTCHA was incorrect, please try again.')
end

When('I complete the captcha') do
  Recaptcha.configuration.skip_verify_env = 'test'
  visit '/register'
  fill_in('user[name]', with: 'Joseph Mercie2')
  fill_in('user[email]', with: 'joseph2@mercie.com')
  fill_in('user[password]', with: 'Passw0rd!1')
  fill_in('user[password_confirmation]', with: 'Passw0rd!1')
end

Then('the registration succeeds') do
  click_button('Create')
  page.should have_content('User created')
end
