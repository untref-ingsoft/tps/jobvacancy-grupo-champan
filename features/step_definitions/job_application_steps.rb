Given(/^only a "(.*?)" offer exists in the offers list$/) do |job_title|
  @job_offer = JobOffer.new(title: job_title, location: 'a nice job', description: 'a nice job', offer_salary: 2000)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

When(/^I access the offers list page$/) do
  visit '/job_offers'
end

Then(/^I should receive a mail with offerer info$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_offer.owner.name).should be true
end

Then('I provide {string} as a link to my CV') do |cv_url|
  click_link 'Apply'
  fill_in('job_application_form[applicant_email]', with: 'applicant@test.com')
  fill_in('job_application_form[cv_url]', with: cv_url)
  click_button('Apply')
end

Given('I am logged in') do
  email = 'logged@test.com'
  password = 'Passw0rd!'
  user = User.new(name: 'Joe', email: email, password: password)
  UserRepository.new.save(user)
  visit '/login'
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: password)
  click_button('Login')
end

When('I apply to the {string} offer') do |_string|
  click_link 'Apply'
end

When('I fill the required information and apply') do
  fill_in('job_application_form[applicant_email]', with: 'applicant@test.com')
  fill_in('job_application_form[cv_url]', with: 'https://someUrl.com')
  click_button('Apply')
end

Given('I am not logged in') do
end

Then('I should be redirected to the login page') do
  current_path = URI.parse(current_url).path
  current_path.should == '/login'
end
