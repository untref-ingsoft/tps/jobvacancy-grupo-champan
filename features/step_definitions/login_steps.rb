Given('I\'m registered with email {string} and my password is {string}') do |email, password|
  user = User.new(name: 'Joe', email: email, password: password)
  UserRepository.new.save(user)
  @email = email
  @password = password
end

When('I log in with the email {string} and password {string} {int} times') do |email, password, attempts|
  visit '/login'
  attempts.times do
    fill_in('user[email]', with: email)
    fill_in('user[password]', with: password)
    click_button('Login')
  end
end

Then('I should be notified that my account has been blocked for 24 hours') do
  page.should have_content('Too many attempts. The account has been blocked for 24 hours')
end

And('I shouldn\'t be able to login') do
  visit '/login'
  fill_in('user[email]', with: @email)
  fill_in('user[password]', with: @password)
  click_button('Login')
  page.should have_content('Your account has been blocked temporarily')
end

Then('I login with email {string} and password {string} succesfuly') do |email, password|
  visit '/login'
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: password)
  click_button('Login')
  page.should have_content(email)
end

Given('I login with email {string} and password {string}') do |email, password|
  visit '/login'
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: password)
  click_button('Login')
end

Given('I log out') do
  visit '/logout'
end

Given('My account was blocked more than {int} hours ago') do |hours|
  user = UserRepository.new.find_by_email(@email)
  user.locked = Time.now - (hours * 60 * 60)
  UserRepository.new.save(user)
end

When('I log in with the email {string} and password {string}') do |email, password|
  visit '/login'
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: password)
  click_button('Login')
end

Then('I login succesfuly') do
  page.should have_content(@email)
end
