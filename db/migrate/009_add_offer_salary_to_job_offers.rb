Sequel.migration do
  up do
    add_column :job_offers, :offer_salary, Integer
  end

  down do
    drop_column :job_offers, :offer_salary
  end
end
