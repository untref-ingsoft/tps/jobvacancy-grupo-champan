Sequel.migration do
  up do
    add_column :users, :attempts, Integer
    add_column :users, :locked, Date
  end

  down do
    drop_column :users, :attempts
    drop_column :users, :locked
  end
end
