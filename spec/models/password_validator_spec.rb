require 'spec_helper'

describe PasswordValidator do
  describe 'valid?' do
    it 'is a valid password' do
      password_validator = described_class.new('Passw0rd!', 'Passw0rd!')
      is_valid = password_validator.valid?
      expect(is_valid).to eq true
    end

    it 'invalid lenght for password' do
      password_validator = described_class.new('Ab34567', 'Ab34567')
      is_valid = password_validator.valid?
      expect(is_valid).to eq false
    end

    it 'is missing uppercase letters' do
      password_validator = described_class.new('ab3456799999', 'ab3456799999')
      is_valid = password_validator.valid?
      expect(is_valid).to eq false
    end

    it 'is missing lowercase letters' do
      password_validator = described_class.new('BA3456799999', 'BA3456799999')
      is_valid = password_validator.valid?
      expect(is_valid).to eq false
    end

    it 'is missing numbers' do
      password_validator = described_class.new('asdASDGTqwe', 'asdASDGTqwe')
      is_valid = password_validator.valid?
      expect(is_valid).to eq false
    end
  end

  describe 'failed_validation' do
    it 'is too short' do
      password_validator = described_class.new('Ab34567', 'Ab34567')
      error = password_validator.failed_validation
      expect(error).to eq [:too_short]
    end

    it 'is missing uppercase letters' do
      password_validator = described_class.new('ab3456799999', 'ab3456799999')
      error = password_validator.failed_validation
      expect(error).to eq [:missing_uppercase_letter]
    end

    it 'is missing lowercase letters' do
      password_validator = described_class.new('AB3456799999', 'AB3456799999')
      error = password_validator.failed_validation
      expect(error).to eq [:missing_lowercase_letter]
    end

    it 'is missing numbers' do
      password_validator = described_class.new('asdASDGTqwe', 'asdASDGTqwe')
      error = password_validator.failed_validation
      expect(error).to eq [:missing_number]
    end

    it 'password and confirmation do not match' do
      password_validator = described_class.new('Passw0rd!', 'Passw0rd!@')
      error = password_validator.failed_validation
      expect(error).to eq [:confirmation_mismatch]
    end
  end
end
