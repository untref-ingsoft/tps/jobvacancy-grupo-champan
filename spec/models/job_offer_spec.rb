require 'spec_helper'

describe JobOffer do
  let(:offer_salary) { 2000 }
  let(:offer_title) { 'Ruby Developer' }
  let(:offer_location) { 'Berlin' }

  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.new(location: offer_location, offer_salary: offer_salary)
      end
    end

    it 'should be valid when title and offer_salary is not blank' do
      job_offer = described_class.new(title: offer_title, offer_salary: offer_salary)
      expect(job_offer).to be_valid
    end

    it 'should be invalid when salary is blank' do
      check_validation(:offer_salary, "Offer salary can't be blank") do
        described_class.new(location: offer_location, title: offer_title)
      end
    end
  end
end
