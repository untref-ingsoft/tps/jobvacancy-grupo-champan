require 'recaptcha'

JobVacancy::App.controllers :users do
  include Recaptcha::Adapters::ControllerMethods
  include Recaptcha::Adapters::ViewMethods

  get :new, map: '/register' do
    @user = User.new
    render 'users/new'
  end

  post :create do
    password_confirmation = params[:user][:password_confirmation]
    params[:user].reject! { |k, _| k == 'password_confirmation' }
    @user = User.new(params[:user])
    password_validator =
      PasswordValidator.new(params[:user][:password], password_confirmation)

    if UserRepository.new.find_by_email(@user.email)
      flash[:error] =
        'Registration failed because the email is already registered'
      redirect '/register'
    elsif password_validator.valid?
      if verify_recaptcha
        if UserRepository.new.save(@user)
          flash[:success] = 'User created'
          redirect '/'
        else
          flash.now[:error] = 'All fields are mandatory'
        end
      else
        flash.now[:error] = 'reCAPTCHA was incorrect, please try again.'
      end
    else
      flash.now[:error] = create_error_message(password_validator)
    end
    render 'users/new'
  end
end
