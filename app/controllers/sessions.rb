JobVacancy::App.controllers :sessions do
  get :login, map: '/login' do
    @user = User.new
    render 'sessions/new'
  end

  post :create do
    email = params[:user][:email]
    password = params[:user][:password]

    gate_keeper = GateKeeper.new.authenticate(email, password)

    gate_keeper.when_succeed do |user|
      @user = user
      sign_in @user
      @user.attempts = 0
      UserRepository.new.save(@user)
      redirect '/'
    end

    gate_keeper.when_failed do |user|
      @user = user
      UserRepository.new.save(@user)
      flash[:error] = if @user.attempts > 3
                        'Your account has been blocked temporarily'
                      elsif @user.locked?
                        'Too many attempts. The account has been blocked for 24 hours'
                      else
                        'Invalid credentials'
                      end
      redirect '/login'
    end
  end

  get :destroy, map: '/logout' do
    sign_out
    redirect '/'
  end
end
