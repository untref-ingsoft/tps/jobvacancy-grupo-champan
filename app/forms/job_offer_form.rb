class JobOfferForm
  attr_accessor :id, :title, :location, :description, :offer_salary

  def self.from(a_job_offer)
    form = JobOfferForm.new
    form.id = a_job_offer.id
    form.title = a_job_offer.title
    form.location = a_job_offer.location
    form.description = a_job_offer.description
    form.offer_salary = a_job_offer.offer_salary
    form
  end
end
