PASSWORD_ERROR_DICTIONARY = {
  too_short: 'The password must be at least 8 characters long.',
  missing_uppercase_letter: 'Please use at least 1 uppercase letter.',
  missing_lowercase_letter: 'Please use at least 1 lowercase letter.',
  missing_number: 'Please use at least 1 number.',
  confirmation_mismatch: 'The password confirmation does not match.'
}.freeze

JobVacancy::App.helpers do
  def create_error_message(password_validator)
    password_validator
      .failed_validation
      .reduce('') do |message, key|
        "#{message} #{PASSWORD_ERROR_DICTIONARY[key]}"
      end
  end
end
