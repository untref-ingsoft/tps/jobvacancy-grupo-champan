class GateKeeper
  def initialize
    @auth_succeed = false
  end

  def authenticate(email, password)
    @user = UserRepository.new.find_by_email(email)
    @auth_succeed = true if @user&.has_password?(password) && !@user.locked?
    self
  end

  def when_succeed
    yield(@user) if @auth_succeed
    self
  end

  def when_failed
    @user.attempts += 1
    @user.locked = Time.new + (60 * 60 * 24) if @user.attempts >= 3
    yield(@user) unless @auth_succeed
    self
  end
end
