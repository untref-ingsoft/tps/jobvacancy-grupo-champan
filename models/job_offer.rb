class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title, :offer_salary,
                :location, :description, :is_active,
                :updated_on, :created_on

  validates :title, :offer_salary, presence: true

  def initialize(data = {})
    @id = data[:id]
    @offer_salary = data[:offer_salary]
    @title = data[:title]
    @location = data[:location]
    @description = data[:description]
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
    validate!
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end
end
