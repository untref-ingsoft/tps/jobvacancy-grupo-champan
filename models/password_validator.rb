class PasswordValidator
  UPPERCASE_LETTER = /[[:upper:]]/.freeze
  LOWERCASE_LETTER = /[[:lower:]]/.freeze
  NUMBERS = /[[:digit:]]/.freeze
  def initialize(password, password_confirmation)
    @reasons = []
    @reasons << :too_short unless password.length >= 8
    @reasons << :missing_uppercase_letter unless UPPERCASE_LETTER.match?(password)
    @reasons << :missing_lowercase_letter unless LOWERCASE_LETTER.match?(password)
    @reasons << :missing_number unless NUMBERS.match?(password)
    @reasons << :confirmation_mismatch unless password == password_confirmation
  end

  def valid?
    @reasons.empty?
  end

  def failed_validation
    @reasons
  end
end
